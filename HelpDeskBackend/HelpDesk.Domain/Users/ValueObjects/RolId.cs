﻿using HelpDesk.Domain.Common.Models;
using HelpDesk.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Users.ValueObjects
{
    public sealed class RolId : ValueObject
    {
        public Guid Value { get; set; }

        public RolId(Guid value)
        {
            Value = value;
        }

        public static RolId Create(Guid value)
        {
            return new RolId(value);
        }

        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
