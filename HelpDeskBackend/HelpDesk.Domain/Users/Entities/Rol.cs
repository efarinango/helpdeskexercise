﻿using HelpDesk.Domain.Common.Models;
using HelpDesk.Domain.Users.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Users.Entities
{
    public class Rol: Entity<RolId>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Rol(string name, string description): base(RolId.Create(Guid.NewGuid()))
        {
            Name = name;
            Description = description;
        }

        public static Rol Create(
            string name, string description)
        {
            return new(name, description);
        }
    }
}
