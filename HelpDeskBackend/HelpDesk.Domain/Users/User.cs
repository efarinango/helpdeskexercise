﻿using HelpDesk.Domain.Common.Models;
using HelpDesk.Domain.Tickets;
using HelpDesk.Domain.Tickets.Entities;
using HelpDesk.Domain.Tickets.ValueObjects;
using HelpDesk.Domain.Users.Entities;
using HelpDesk.Domain.Users.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Users
{
    public class User : Entity<UserId>
    {
        private readonly List<Ticket> _tickets = new List<Ticket>();
        public Rol Rol { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }

        public IReadOnlyList<Ticket> Tickets => _tickets.AsReadOnly();

        public User(string FirstName, string LastName, string Email, string Password)
         :base(UserId.Create(Guid.NewGuid()))
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Email = Email;
            this.Password = Password;
            CreatedAt = DateTime.Now;
        }

        public static User Create(
            string firstName, string lastName, string email, string password)
        {
            return new(firstName, lastName, email, password);
        }


    }
}
