﻿using HelpDesk.Domain.Common.Models;
using HelpDesk.Domain.Tickets.Entities;
using HelpDesk.Domain.Tickets.ValueObjects;
using HelpDesk.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Tickets
{
    public sealed class Ticket: Entity<TicketId>
    {
        public User CreatedBy { get; set; }
        
        public string Title { get; set; }

        public string Description { get; set; }


        //public IReadOnlyList<TicketAtention> TicketAtentions => _ticketAtentions.AsReadOnly();

        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdatedDate { get; set;}

        private Ticket(
            TicketId ticketId,
            string title, string description)
            : base(ticketId)
        {
            Title = title;
            Description = description;
        }

        public static Ticket Create(
            string title, string description)
        {
            return new(TicketId.Create(Guid.NewGuid()),
                title, description);
        }
    }
}
