﻿using HelpDesk.Domain.Common.Models;
using HelpDesk.Domain.Tickets.ValueObjects;
using HelpDesk.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Tickets.Entities
{
    public class TicketAtention: Entity<TicketAtentionId>
    {
        //private readonly List<User> _users = new();
        public string Title { get; set; }

        public string Description { get; set; }

        //public IReadOnlyList<User> Users => _users.ToList();

        private TicketAtention(
            TicketAtentionId ticketAtentionId,
            string title, string description)
            : base(ticketAtentionId)
        {
            Title = title;
            Description = description;
        }

        public static TicketAtention Create(
            string title, string description)
        {
            return new(TicketAtentionId.CreateUnique(),
                title, description);
        }
    }
}
