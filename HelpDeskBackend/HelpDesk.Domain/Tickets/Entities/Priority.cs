﻿using HelpDesk.Domain.Common.Models;
using HelpDesk.Domain.Tickets.ValueObjects;
using HelpDesk.Domain.Users.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Tickets.Entities
{
    public class Priority : Entity<PriorityId>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public int Level { get; set; }

        public Priority(string name, string description, int level) : base(PriorityId.Create(Guid.NewGuid()))
        {
            Name = name;
            Description = description;
            Level = level;
        }

        public static Priority Create(
            string name, string description, int level)
        {
            return new(name, description, level);
        }
    }
}
