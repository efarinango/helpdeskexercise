﻿using HelpDesk.Domain.Common.Models;
using HelpDesk.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Tickets.ValueObjects
{
    public sealed class PriorityId : ValueObject
    {
        public Guid Value { get; set; }

        public PriorityId(Guid value)
        {
            Value = value;
        }

        public static PriorityId Create(Guid value)
        {
            return new PriorityId(value);
        }

        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
