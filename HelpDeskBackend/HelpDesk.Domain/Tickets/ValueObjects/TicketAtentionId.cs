﻿using HelpDesk.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Tickets.ValueObjects
{
    public sealed class TicketAtentionId : ValueObject
    {
        public Guid Value { get; }

        public TicketAtentionId(Guid value)
        {
            Value = value;
        }

        public static TicketAtentionId CreateUnique()
        {
            return new (Guid.NewGuid());
        }

        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
