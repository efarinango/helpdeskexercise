﻿using HelpDesk.Domain.Common.Models;
using HelpDesk.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Tickets.ValueObjects
{
    public sealed class TicketId : ValueObject
    {
        public Guid Value { get; }

        public TicketId(Guid value)
        {
            Value = value;
        }

        public static TicketId Create(Guid value)
        {
            return new (value);
        }

        public override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
