﻿using ErrorOr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Common.Errors
{
    public static partial class Errors
    {
        public static class Login
        {
            public static Error InvalidCredentials =
                Error.Validation(
                    code: "Login.InvalidCredentials", 
                    description: "InvalidCredentials");
        }
    }
}
