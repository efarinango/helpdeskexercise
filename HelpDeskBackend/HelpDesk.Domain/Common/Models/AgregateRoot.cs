﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Domain.Common.Models
{
    public class AgregateRoot<TId> : Entity<TId> 
        where TId : notnull
    {
        protected AgregateRoot(TId id) : base(id) 
        { 

        }

    }
}
