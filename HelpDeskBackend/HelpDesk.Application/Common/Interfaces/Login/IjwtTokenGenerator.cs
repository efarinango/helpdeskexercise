﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Application.Common.Interfaces.Login
{
    public interface IjwtTokenGenerator
    {
        string GenerateToken(Guid userId, string firstName, string lastName);
    }
}
