﻿using ErrorOr;
using FluentValidation;
using HelpDesk.Application.Users.Commands.Create;
using HelpDesk.Application.Users.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Application.Common.Behaviours
{
    public class ValidationBehaviour<TRequest, TResponse>:
        IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
        where TResponse: IErrorOr
    {
        private readonly IValidator<TRequest> _validator;

        public ValidationBehaviour(IValidator<TRequest>? validator = null)
        {
            _validator = validator;
        }

        public async Task<TResponse> Handle(
            TRequest request, 
            RequestHandlerDelegate<TResponse> next, 
            CancellationToken cancellationToken)
        {
            if(_validator is null) {
                return await next();
            }
            var validationResult = await _validator.ValidateAsync(request, cancellationToken);
            //Before handler
            
            if (validationResult.IsValid) {
                return await next();
            }

            var errors = validationResult.Errors
                .ConvertAll(vf => Error.Validation(
                    vf.PropertyName, vf.ErrorMessage));

            return (dynamic)errors;
        }
    }

}
