﻿using ErrorOr;
using HelpDesk.Application.Users.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Application.Users.Commands.Create
{
    public record RegisterCommand( 
        string FirstName, 
        string LastName, 
        string Email, 
        string Password): IRequest<ErrorOr<CreateResult>>;
}
