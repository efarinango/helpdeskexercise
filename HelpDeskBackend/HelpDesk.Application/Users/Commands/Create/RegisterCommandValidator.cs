﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Application.Users.Commands.Create
{
    public class RegisterCommandValidator: AbstractValidator
        <RegisterCommand>
    {

        public RegisterCommandValidator() { 
            RuleFor(x=> x.FirstName).NotEmpty();
            RuleFor(x=> x.LastName).NotEmpty();
            RuleFor(x=> x.Email).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
