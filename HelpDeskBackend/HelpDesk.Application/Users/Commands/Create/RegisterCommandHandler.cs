﻿using ErrorOr;
using HelpDesk.Application.Common.Interfaces.Persistence;
using HelpDesk.Application.Users.Common;
using HelpDesk.Domain;
using HelpDesk.Domain.Common.Errors;
using HelpDesk.Domain.Users;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Application.Users.Commands.Create
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, ErrorOr<CreateResult>>
    {
        private readonly IUserRepository _userRepository;
        
        public RegisterCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<ErrorOr<CreateResult>> Handle(RegisterCommand command, CancellationToken cancellationToken)
        {
            if (_userRepository.GetUserByEmail(command.Email) != null)
            {
                return Errors.User.DuplicateEmail;
            }

            var user = new User(
                command.FirstName,
                command.LastName,
                command.Email,
                command.Password
            );
            _userRepository.Create(user);

            return new CreateResult(
                user.Id,
                user.FirstName,
                user.LastName);
        }
    }
}
