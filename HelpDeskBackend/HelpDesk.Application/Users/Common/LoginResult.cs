using HelpDesk.Domain.Tickets.ValueObjects;
using HelpDesk.Domain.Users.ValueObjects;

namespace HelpDesk.Application.Users.Common;

public record LoginResult(
    UserId Id,
    string FirstName,
    string LastName,
    string Email,
    string Token);

public record CreateResult(
    UserId Id,
    string FirstName,
    string LastName);