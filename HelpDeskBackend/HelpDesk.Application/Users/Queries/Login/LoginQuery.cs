﻿using ErrorOr;
using HelpDesk.Application.Users.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Application.Users.Queries.Login
{
    public record LoginQuery(
        string Email, 
        string Password): IRequest<ErrorOr<LoginResult>> ;
}
