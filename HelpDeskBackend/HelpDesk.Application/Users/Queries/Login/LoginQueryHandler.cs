﻿using ErrorOr;
using HelpDesk.Application.Common.Interfaces.Persistence;
using HelpDesk.Application.Users.Commands.Create;
using HelpDesk.Application.Users.Common;
using HelpDesk.Domain;
using HelpDesk.Domain.Common.Errors;
using HelpDesk.Domain.Users;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Application.Users.Queries.Login
{
    public class LoginQueryHandler : 
        IRequestHandler<LoginQuery, ErrorOr<LoginResult>>
    {
        private readonly IUserRepository _userRepository;

        public LoginQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<ErrorOr<LoginResult>> Handle(LoginQuery query, CancellationToken cancellationToken)
        {
            if (_userRepository.GetUserByEmail(query.Email) is not User user)
            {
                return Errors.Login.InvalidCredentials;
            }

            if (user.Password != query.Password)
            {
                return Errors.Login.InvalidCredentials;
            }

            return new LoginResult(
                user.Id,
                user.FirstName,
                user.LastName,
                user.Email,
                "Token"
                );
        }
    }
}
