﻿using FluentValidation;
using HelpDesk.Application.Users.Queries.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Application.Users.Commands.Login
{
    public class LoginQueryValidator: AbstractValidator
        <LoginQuery>
    {

        public LoginQueryValidator() {
            RuleFor(x=> x.Email).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
