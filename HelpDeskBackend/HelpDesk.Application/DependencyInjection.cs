using ErrorOr;
using FluentValidation;
using HelpDesk.Application.Common.Behaviours;
using HelpDesk.Application.Users.Commands.Create;
using HelpDesk.Application.Users.Commands.Login;
using HelpDesk.Application.Users.Common;
using HelpDesk.Application.Users.Queries.Login;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace HelpDesk.Application;

public static class DependencyInjection{

    public static IServiceCollection AddApplicacion(this IServiceCollection services){
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
        services.AddScoped(
            typeof(IPipelineBehavior<,>),
            typeof(ValidationBehaviour<,>)
            );
        services.AddScoped<IValidator
            <RegisterCommand>, RegisterCommandValidator>();
        services.AddScoped<IValidator
            <LoginQuery>, LoginQueryValidator>();
        return services;
    }
}