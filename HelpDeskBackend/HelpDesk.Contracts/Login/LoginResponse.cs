namespace HelpDesk.Contracts.Login;

public record LoginResponse(
    string FirstName,
    string LastName
);

public record CreateResponse(
    //Guid Id,
    string FirstName,
    string LastName
);