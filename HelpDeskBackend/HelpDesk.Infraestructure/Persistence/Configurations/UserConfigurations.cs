﻿using HelpDesk.Domain;
using HelpDesk.Domain.Tickets.ValueObjects;
using HelpDesk.Domain.Users;
using HelpDesk.Domain.Users.Entities;
using HelpDesk.Domain.Users.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Infraestructure.Persistence.Configurations
{
    public class UserConfigurations :
        IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            ConfigureUsersTable(builder);
            //ConfigureRolsTable(builder);
            ConfigureTicketsTable(builder);
        }

        private void ConfigureTicketsTable(EntityTypeBuilder<User> builder)
        {
            builder.OwnsMany(t => t.Tickets, tb =>
            {
                tb.ToTable("hd_tickets");

                tb.WithOwner().HasForeignKey(t => t.Id);

                tb.HasKey(t => t.Id);

                tb.Property(s=> s.Id)
                .ValueGeneratedNever()
                .HasConversion(
                    id=> id.Value,
                    value => TicketId.Create(value));

                tb.Property(s => s.Title)
                .HasMaxLength(20);
            });

            builder.Metadata
                .FindNavigation(nameof(User.Tickets))!
                .SetPropertyAccessMode(PropertyAccessMode.Field);
        }

        private void ConfigureUsersTable(
            EntityTypeBuilder<User> builder)
        {
            builder.ToTable("hd_users");
            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id)
                .ValueGeneratedNever()
                .HasConversion(
                    id => id.Value,
                    value => UserId.Create(value)
                    );

            builder.Property(u => u.FirstName)
                .HasMaxLength(50);

            builder.Property(u => u.LastName)
                .HasMaxLength(50);

        }

        private void ConfigureRolsTable(
            EntityTypeBuilder<User> builder)
        {
            builder.OwnsOne(u => u.Rol, rb =>
            {
                rb.ToTable("hd_rols");
                //rb.WithOwner().HasForeignKey(
                //    u => u.Id);
                rb.Property(r => r.Id)
                .HasColumnName("RolId")
                .ValueGeneratedNever()
                .HasConversion(
                    id => id.Value,
                    value => RolId.Create(value)
                    );
            });
        }
    }
}
