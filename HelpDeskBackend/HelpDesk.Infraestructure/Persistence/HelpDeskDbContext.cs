﻿using HelpDesk.Domain;
using HelpDesk.Domain.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Infraestructure.Persistence
{
    public class HelpDeskDbContext: DbContext
    {
        public HelpDeskDbContext(DbContextOptions<HelpDeskDbContext> options) 
            : base(options) { 
        }


        public DbSet<User> Users { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.
                ApplyConfigurationsFromAssembly(
                typeof(HelpDeskDbContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }


    }
}
