﻿using HelpDesk.Application.Common.Interfaces.Persistence;
using HelpDesk.Domain;
using HelpDesk.Domain.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpDesk.Infraestructure.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private HelpDeskDbContext _dbContext;

        public UserRepository(HelpDeskDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Create(User user)
        {
            _dbContext.Add(user);
            _dbContext.SaveChanges();
        }

        public User? GetUserByEmail(string email)
        {
            //return _dbContext.Users.Find(u => u.Email == email);

            return _dbContext.Users.FirstOrDefault(u => u.Email == email);
        }
    }
}
