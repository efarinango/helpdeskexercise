using HelpDesk.Application.Common.Interfaces.Login;
using HelpDesk.Application.Common.Interfaces.Persistence;
using HelpDesk.Application.Common.Interfaces.Services;
using HelpDesk.Infraestructure.Persistence;
using HelpDesk.Infraestructure.Persistence.Repositories;
using HelpDesk.Infraestructure.Services;
using HelpDesk.Infraestructure.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace HelpDesk.Infraestructure;

public static class DependencyInjection{

    public static IServiceCollection AddInfraestructure(this IServiceCollection services){
        services.AddDbContext<HelpDeskDbContext>(options =>
        options.UseSqlServer(
            "Server=localhost;Database=HelpDeskPrueba;User Id=SA;Password=1234;Encrypt=false"));
        services.AddSingleton<IDateTimeProvider, DateTimeProvider>();
        services.AddScoped<IUserRepository, UserRepository>();
        return services;
    }
}