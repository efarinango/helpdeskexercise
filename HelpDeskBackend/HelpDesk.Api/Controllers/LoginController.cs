using Microsoft.AspNetCore.Mvc;
using HelpDesk.Contracts.Login;
using HelpDesk.Contracts.Users;
using HelpDesk.Api.Filters;
using ErrorOr;
using HelpDesk.Domain.Common.Errors;
using MediatR;
using HelpDesk.Application.Users.Commands.Create;
using HelpDesk.Application.Users.Common;
using HelpDesk.Application.Users.Queries.Login;

namespace HelpDesk.Api.Controllers;


[Route("login")]
public class LoginController: ApiController{

    private readonly ISender _mediator;

    public LoginController(ISender mediator)
    {
        _mediator = mediator;
    }


    [Route("register")]
    public async Task<IActionResult> CreateUser(CreateUserRequest request){
        
        var command = new RegisterCommand(
            request.FirstName, request.LastName, 
            request.Email, request.Password);
        ErrorOr<CreateResult> authresult = await _mediator.Send(command);

        return authresult.Match(
            authresult => Ok(MapCreateResult(authresult)),
            errors => Problem(errors)
            );
    }


    [Route("login")]
    public async Task<IActionResult> Login(LoginRequest request){
        var query = new LoginQuery(
            request.Email,
            request.Password);
        var authresult = await _mediator.Send(query);
        if(authresult.IsError 
            && authresult.FirstError ==  Errors.Login.InvalidCredentials )
        {
            return Problem(statusCode: StatusCodes.Status401Unauthorized);
        }

        return authresult.Match(
            authresult => Ok(MapLoginResult(authresult)),
            errors => Problem(errors));    
    }
    private static LoginResponse MapLoginResult(LoginResult authResult)
    {
        return new LoginResponse(
            "otro valor",
            authResult.Email);
    }


    private static CreateResponse MapCreateResult(CreateResult authResult)
    {
        return new CreateResponse(
            //authResult.Id,
            authResult.FirstName,
            authResult.LastName);
    }
}