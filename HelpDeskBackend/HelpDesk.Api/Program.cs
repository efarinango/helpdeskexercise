using HelpDesk.Api.Common.Errors;
using HelpDesk.Api.Filters;
using HelpDesk.Api.Middleware;
using HelpDesk.Application;
using HelpDesk.Infraestructure;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc.Infrastructure;

var builder = WebApplication.CreateBuilder(args);
{
    // Add services to the container.
    builder.Services.AddApplicacion()
        .AddInfraestructure();
    builder.Services.AddControllers()
        .AddNewtonsoftJson();
    builder.Services.AddSingleton<ProblemDetailsFactory, HelpDeskProblemDetailsFactory>();
    builder.Services.AddMemoryCache();
}



var app = builder.Build();
{
    app.UseExceptionHandler("/error");
    app.UseHttpsRedirection();
    app.MapControllers();

    app.Run();
}



